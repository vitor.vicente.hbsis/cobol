      $set preprocess (htmlpp) endp
      $set sourceformat"free"

      *>===================================================================================
       identification division.
       program-id.    ACESSO.
      *>===================================================================================

      *>   -----------------------------------------------------
      *>               Programa Principal
      *>   -----------------------------------------------------

      *>===================================================================================
       environment division.
       special-names. decimal-point is comma.
      *>===================================================================================
       input-output section.
       file-control.

       *> Log de login no sistema
       select arq-log assign to disk wid-arq-log
           organization    is line sequential
           access mode     is sequential
           file status     is ws-resultado-acesso.

      *>===================================================================================
       data division.

       fd arq-log.
       01 registro-arq-log.
           03 arq-log-data             pic x(23).  *>14/02/2020;17:04:34:85;
           03 arq-log-ip-cliente       pic x(100).
           03 arq-log-texto            pic x(2000).

      *>===================================================================================
       working-storage section.
       78 versao                       value "3.0".
       78 path-projeto                 value "D:\DEV\Prova-Cobol\cobol".
       78 path-copys                   value "\copys\".
       78 path-log                     value "\log\".
       78 path-data                    value "\dat\".
       78 path-import                  value "\imp\".
       78 path-tmp                     value "\tmp\".

       copy .\copys\WS-PADRAO.cpy.
       copy .\copys\LNK.cpy.

       01 campos-de-trabalho.
           02 ws-data-maquina-format       pic x(10).
           02 filler redefines ws-data-maquina-format.
               03 ws-maq-dia-format        pic 9(02).
               03 filler                   pic x(01).
               03 ws-maq-mes-format        pic 9(02).
               03 filler                   pic x(01).
               03 ws-maq-ano-format        pic 9(04).
           02 ws-hora-maquina-format       pic x(11).
           02 filler redefines ws-hora-maquina-format.
               03 ws-maq-hora-format       pic 9(02).
               03 filler                   pic x(01).
               03 ws-maq-minu-format       pic 9(02).
               03 filler                   pic x(01).
               03 ws-maq-segu-format       pic 9(02).
               03 filler                   pic x(01).
               03 ws-maq-mili-format       pic 9(02).

      *>===================================================================================
       local-storage section.

      *>===================================================================================
       procedure division.

      *>===================================================================================
       0000-controle section.
       0000.
            perform 1000-inicializacao
            perform 2000-processamento
            perform 3000-finalizacao
            .
       0000-saida.
            exit program
            stop run.

      *>===================================================================================
       1000-inicializacao section.
       1000.

            move spaces                             to cgi-input
            initialize                              cgi-input
            accept                                  cgi-input

            accept lnk-data-maquina                  from date YYYYMMDD
            accept lnk-hora-maquina                  from time
            perform 1001-data-hora-format

            perform 1010-path-arquivos
            perform 1011-verifica-path
            perform 1020-assign-arquivos
            perform 1021-verifica-arquivos
            perform 1031-arquivo-log-sistema
            .
       1000-exit.
            exit.
      *>===================================================================================
       1001-data-hora-format section.
       1001.

           string
               lnk-maq-dia delimited by " ", "/",
               lnk-maq-mes delimited by " ", "/",
               lnk-maq-ano delimited by " ", "/",
           into ws-data-maquina-format
           end-string

           string
               lnk-maq-hora delimited by " ", ":",
               lnk-maq-minu delimited by " ", ":",
               lnk-maq-segu delimited by " ", ":",
               lnk-maq-mili delimited by " ", ":",
           into ws-hora-maquina-format
           end-string

           .
       1001-exit.
            exit.
      *>===================================================================================
       1010-path-arquivos section.
       1010.

           move spaces                             to lnk-path

           string
               path-projeto, path-copys
               into lnk-path-copys
           end-string

           string
               path-projeto, path-log
               into lnk-path-log
           end-string

           string
               path-projeto, path-data
               into lnk-path-dat
           end-string

           string
               path-projeto, path-import
               into lnk-path-import
           end-string

           string
               path-projeto, path-tmp
               into lnk-path-tmp
           end-string

           .
       1010-exit.
            exit.
      *>===================================================================================
       1011-verifica-path section.
       1011.
            *>cria as pastas caso n�o existam
            call "CBL_CREATE_DIR"       using lnk-path-log
                                    returning ws-status-code

            call "CBL_CREATE_DIR"       using lnk-path-copys
                                    returning ws-status-code

            call "CBL_CREATE_DIR"       using lnk-path-dat
                                    returning ws-status-code

            call "CBL_CREATE_DIR"       using lnk-path-import
                                    returning ws-status-code

            call "CBL_CREATE_DIR"       using lnk-path-tmp
                                    returning ws-status-code
            .
       1011-exit.
            exit.
      *>===================================================================================
       1020-assign-arquivos section.
       1020.

           string
               lnk-path-log delimited by " ",
               "arq-log",                "-",
               lnk-data-maquina,          ".csv"
               into wid-arq-log
           end-string

           .
       1020-exit.
            exit.
      *>===================================================================================
       1021-verifica-arquivos section.
       1021.

           call "CBL_CHECK_FILE_EXIST"     using wid-arq-log
                                                 ws-file-details
                                       returning ws-status-code

           if ws-status-code <> zeros
               open output arq-log
               close arq-log
           end-if

           .
       1021-exit.
            exit.
      *>===================================================================================
       1031-arquivo-log-sistema section.
       1031.

           open i-o arq-log
           read arq-log
           if ws-eof-arquivo

               close arq-log
               open extend arq-log

               string
                   "Data",             ";",
                   "Hora",             ";",
                   "IP cliente",       ";",
                   "log"
               into registro-arq-log
               end-string
               write registro-arq-log

               move "Primeiro login no sistema - Arquivo de LOG criado"
                   to arq-log-texto
               perform 1032-grava-log-nova-linha

           else
               move "Acessou programa principal"   to arq-log-texto
               perform 1032-grava-log-nova-linha
           end-if

           close arq-log
           .
       1031-exit.
            exit.
      *>===================================================================================
       1032-grava-log-nova-linha section.
       1032.

           perform 1033-get-ip-cliente

           close arq-log
           open extend arq-log
           string
               ws-data-maquina-format      delimited by " ", ";"
               ws-hora-maquina-format      delimited by " ", ";"
               arq-log-ip-cliente          delimited by " ", ";"
               arq-log-texto
           into registro-arq-log
           end-string

           write registro-arq-log
           move spaces                             to registro-arq-log

           .
       1032-exit.
            exit.
      *>===================================================================================
       1033-get-ip-cliente section.
       1033.

           move spaces                             to arq-log-ip-cliente

           display "REMOTE_ADDR" upon environment-name
           end-display

           accept arq-log-ip-cliente from environment-value
           end-accept

           .
       1033-exit.
            exit.
      *>===================================================================================
       2000-processamento section.
       2000.

            evaluate f-opcao
                when 0
                    perform 2010-tela-inicial
                when 1
                    perform 2020-verifica-login
                when other
                    move "Op��o inv�lida"           to ws-mensagem
                    perform 8000-mensagem
            end-evaluate

            .
       2000-exit.
            exit.
      *>===================================================================================
       2010-tela-inicial section.
       2010.

       exec html
           <html><title>Tela principal</title>
       end-exec

            perform 9000-css-pagina
            perform 2012-campos-login
            perform 2013-scripts-tela

       exec html
           </html>
       end-exec
           .
       2010-exit.
            exit.
      *>===================================================================================
       2012-campos-login section.
       2012.

       exec html
           <form name=formLogar method=post>
           <input type=hidden name=opcao        value=:f-opcao>
           <input type=hidden name=opcaoMenus   value=:f-opcao-menus>

           <table :html-table>

               <tr>
               <td colspan=2>
                   <p class="txtSuperior"> Login do sistema </p>
               </td>
               </tr>

               <tr>
                   <td>Login:</td>
                   <td><input type=text name=login></td>
               </tr>

               <tr>
                   <td>Senha:</td>
                   <td><input type=password name=senha /></td>
               </tr>

               <tr>
                   <td colspan=2>&nbsp</td>
               </tr>

               <tr>
                   <td colspan=2><input type=button name=botLogar value=Logar onclick="Logar()"></td>
               </tr>
           </table>

           </form>
           <!-- ================================ fim formLogar ================================ -->

       end-exec

           .
       2012-exit.
            exit.
      *>===================================================================================
       2013-scripts-tela section.
       2013.

       exec html
           <script>
               function Logar(){
                   document.all.botLogar.disabled = true;
                   document.formLogar.opcao.value = 1;
                   document.formLogar.submit();
               }
           </script>
       end-exec


           .
       2013-exit.
            exit.
      *>===================================================================================
       2020-verifica-login section.
       2020.
            if not f-user-logado

                   if   f-login <> "admin"
                   and  f-senha <> "admin"
                       move "Login ou senha inv�lidos"
                           to ws-mensagem
                       perform 8000-mensagem
                       perform 2010-tela-inicial
                       exit section
                   end-if

            end-if

            move "S"                               to f-status-login
            perform 2021-abre-form-menus

            call "MENUS" using cgi-input lnk-rotinas
            cancel "MENUS"

            perform 2022-fecha-form-menus
            .
       2020-exit.
            exit.
      *>===================================================================================
       2021-abre-form-menus section.
       2021.

       exec html

           <form name=formMenus method=post>
           <input type=hidden name=opcao           value=:f-opcao>
           <input type=hidden name=opcaoMenus      value=:f-opcao-menus>
           <input type=hidden name=opcaoCadCli     value=:f-opcao-cad-cli>
           <input type=hidden name=opcaoCadVend    value=:f-opcao-cad-vend>
           <input type=hidden name=opcaoRelCliente value=:f-opcao-rel-cliente>
           <input type=hidden name=userLogado      value=:f-status-login>

       end-exec

            .
       2021-exit.
            exit.
      *>===================================================================================
       2022-fecha-form-menus section.
       2022.

       exec html

       </form>
       <!-- ================================ fim formMenus ================================ -->

       end-exec

            .
       2022-exit.
            exit.

      *>===================================================================================
       3000-finalizacao section.
       3000.

            perform 3010-controles-de-tela


            .
       3000-exit.
            exit.
      *>===================================================================================
       3010-controles-de-tela section.
       3000.

       exec html
       <script>

           if( ':f-opcao' == '00' ) {
               document.all.login.focus();
           }

       </script>
       end-exec

            .
       3010-exit.
            exit.
      *>===================================================================================

       copy .\copys\ROTINAS-PADRAO.cpy.

