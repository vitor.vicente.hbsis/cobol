      $set preprocess (htmlpp) endp
      $set sourceformat"free"

      *>===================================================================================
       identification division.
       program-id.    CadCliente.
      *>===================================================================================

      *>   -----------------------------------------------------
      *>               Programa Exemplo
      *>   -----------------------------------------------------

      *>===================================================================================
       environment division.
       special-names. decimal-point is comma.
      *>===================================================================================
       input-output section.
       file-control.

       select arq-clientes assign to disk wid-arq-clientes
           organization is indexed
           record key is cod-cliente
           file status is ws-resultado-acesso.

      *>===================================================================================
       data division.

       fd arq-clientes.
       01 registro-clientes.
           02 cod-cliente                          pic 9(07).
           02 cnpj                                 pic 9(14).
           02 razao-social                         pic x(40).
           02 latitude                             pic s9(003)v9(008).
           02 longitude                            pic s9(003)v9(008).

      *>===================================================================================
       working-storage section.
       78 versao                       value "1.5".

       copy .\copys\WS-PADRAO.cpy.
       copy .\copys\LNK.cpy.

      *>===================================================================================
       local-storage section.

      *>===================================================================================
       procedure division using cgi-input lnk-rotinas.

      *>===================================================================================
       0000-controle section.
       0000.
            perform 1000-inicializacao.
            perform 2000-processamento.
            perform 3000-finalizacao.
       0000-saida.
            exit program
            stop run.

      *>===================================================================================
       1000-inicializacao section.
       1000.

            move spaces                             to cgi-input
            initialize                              cgi-input
            accept                                  cgi-input

            perform 1010-assign-arquivos
            perform 1011-cricao-arquivos
            perform 1012-abertuta-arquivos
            .
       1000-exit.
            exit.
      *>===================================================================================
       1010-assign-arquivos section.
       1010.

            string
               lnk-path-dat    delimited by " ",
               "ARQ-CLIENTES"  delimited by " "
            into wid-arq-clientes
            end-string
            .
       1010-exit.
            exit.
      *>===================================================================================
       1011-cricao-arquivos section.
       1011.

            open i-o arq-clientes
            close arq-clientes

            .
       1010-exit.
            exit.
      *>===================================================================================
       1012-abertuta-arquivos section.
       1012.

            open i-o arq-clientes
            if not ws-operacao-ok

                   string
                       "Erro abertura do arquivo arq-clientes, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string

                   perform 8000-mensagem
                   perform 0000-saida
            end-if

            .
       1012-exit.
            exit.
      *>===================================================================================
       2000-processamento section.
       2000.

            evaluate f-opcao-cad-cli
            when 0
               perform 2010-monta-tela-inicial
            when other
               move "opcao inv�lida" to ws-mensagem
               perform 8000-mensagem
            end-evaluate


            .
       2000-exit.
            exit.
      *>===================================================================================
       2010-monta-tela-inicial section.
       2010.

       exec html
           <html><title>Cadastro de Clientes</title>
       end-exec

            perform 9000-css-pagina
            perform 2012-monta-tabela
            perform 2013-scripts-tela

       exec html
           </html>
       end-exec
            .
       2010-exit.
            exit.
      *>===================================================================================
       2012-monta-tabela section.
       2012.

       exec html
           <table :html-table>
           <tr>
               <td><h1>CADASTRO DE CLIENTES</h1></td>
               <td> </td>
           </tr>

           <tr>
               <td></td>
           </tr>

           </table>

       end-exec
            .
       2012-exit.
            exit.
      *>===================================================================================
       2013-scripts-tela section.
       2013.

            .
       2013-exit.
            exit.
      *>===================================================================================
       3000-finalizacao section.
       3000.

            perform 3010-fechamento-arquivos

            .
       3000-exit.
            exit.
      *>===================================================================================
       3010-fechamento-arquivos section.
       3010.

            close arq-clientes

            .
       3010-exit.
            exit.

       copy .\copys\ROTINAS-PADRAO.cpy.
