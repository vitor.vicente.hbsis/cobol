      $set preprocess (htmlpp) endp
      $set sourceformat"free"

      *>===================================================================================
       identification division.
       program-id.    CadVendedor.
      *>===================================================================================

      *>   -----------------------------------------------------
      *>               Programa cad vendedores
      *>   -----------------------------------------------------

      *>===================================================================================
       environment division.
       special-names. decimal-point is comma.
      *>===================================================================================
       input-output section.
       file-control.

       copy ./copys/SEL-ARQ-VENDEDORES.

       select arq-vendedores-importacao assign to disk wid-arq-vendedores-importacao
           organization    is line sequential
           access mode     is sequential
           file status     is ws-resultado-acesso.

       select arq-vendedores-tmp assign to disk wid-arq-vendedores-tmp
           organization    is line sequential
           access mode     is sequential
           file status     is ws-resultado-acesso.

      *>===================================================================================
       data division.

       copy ./copys/FD-ARQ-VENDEDORES.

       fd arq-vendedores-importacao.
       01 imp-registro-clientes.
           02 campos-importacao                    pic x(83).

       fd arq-vendedores-tmp.
       01 tmp-registro-clientes.
           02 tmp-cod-cliente                      pic 9(07).
           02 tmp-cpf                              pic 9(11).
           02 tmp-razao-social                     pic x(40).
           02 tmp-latitude                         pic x(12).
           02 tmp-longitude                        pic x(12).

      *>===================================================================================
       working-storage section.
       78 versao                                   value "1.8".

       copy .\copys\WS-PADRAO.cpy.
       copy .\copys\LNK.cpy.

       01 ws-campos-de-trabalho.
           02 ws-valida-campos                     pic x(01).
               88 ws-campos-ok                     value "s" "S".
           02 ws-campo-erro                        pic x(30).
           02 ws-cod-cliente-livre                 pic 9(07).
           02 ws-encontrou-registro                pic x(01).
           02 ws-valida-arquivo                    pic x(01).
               88 ws-arquivo-ok                    value "s" "S".
           02 ws-qtd-campos-esperados              pic 9(01).
           02 ws-status-importacao                 pic x(01).
               88 ws-importacao-ok                 value "s" "S".
           02 ws-ind                               pic 9(05) value zeros.
           02 ws-latitude-x                        pic x(12).
           02 ws-longitude-x                       pic x(12).
           02 ws-linha-arq                         pic 9(07).



       01 ws-campos-importacao.
           02 imp-cpf-x                            pic x(11).
           02 imp-razao-social-x                   pic x(40).
           02 imp-latitude-x                       pic x(12).
           02 imp-longitude-x                      pic x(12).

           02 imp-cpf                              pic 9(11).
           02 imp-razao-social                     pic x(40).
           02 imp-latitude                         pic x(12).
           02 imp-longitude                        pic x(12).


      *>===================================================================================
       local-storage section.

      *>===================================================================================
       procedure division using cgi-input lnk-rotinas.

      *>===================================================================================
       0000-controle section.
       0000.
            perform 1000-inicializacao.
            perform 2000-processamento.
            perform 3000-finalizacao.
       0000-saida.
            exit program
            stop run.

      *>===================================================================================
       1000-inicializacao section.
       1000.

            move spaces                            to cgi-input
            initialize                             cgi-input
            accept                                 cgi-input

            perform 1010-assign-arquivos
            perform 1011-cricao-arquivos
            perform 1012-teste-abertuta-arquivos
            .
       1000-exit.
            exit.
      *>===================================================================================
       1010-assign-arquivos section.
       1010.

            string
               lnk-path-dat                        delimited by " ",
               "ARQ-VENDEDORES"                    delimited by " "
                                                   into wid-arq-vendedores
            end-string

            string
               lnk-path-import                     delimited by " ",
               f-arquivo                           delimited by " "
                                                   into wid-arq-vendedores-importacao
            end-string

            string
               lnk-path-tmp                        delimited by " ",
               "tmp_importar_"                      delimited by " ",
               f-arquivo                           delimited by " ",
               "_",
               lnk-data-hora-maquina               delimited by " ",
                                                   into wid-arq-vendedores-tmp
            end-string

            .
       1010-exit.
            exit.
      *>===================================================================================
       1011-cricao-arquivos section.
       1011.

            open i-o arq-vendedores
            close arq-vendedores

            .
       1010-exit.
            exit.
      *>===================================================================================
       1012-teste-abertuta-arquivos section.
       1012.

            open i-o arq-vendedores
            if not ws-operacao-ok

                   string
                       "Erro abertura do arquivo arq-vendedores, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 0000-saida
            end-if
            close arq-vendedores

            .
       1012-exit.
            exit.
      *>===================================================================================
       2000-processamento section.
       2000.

            evaluate f-opcao-cad-vend
            when 0
               perform 2010-monta-tela-inicial
            when 1
               perform 2010-monta-tela-inicial
            when 2
               perform 2020-salvar
            when 3
               perform 2030-pesquisar
            when 4
               perform 2040-excluir
            when 5
               perform 2050-importar
            when 6
               perform 2060-exe-importar
            when other
               move "opcao inv�lida" to ws-mensagem
               perform 8000-mensagem
               perform 0000-saida
            end-evaluate

            .
       2000-exit.
            exit.
      *>===================================================================================
       2010-monta-tela-inicial section.
       2010.

       exec html
           <html><title>Cadastro de Vendedores</title>
       end-exec

            perform 9000-css-pagina
            perform 2012-monta-tabela
            perform 2013-scripts-tela

       exec html
           </html>
       end-exec
            .
       2010-exit.
            exit.
      *>===================================================================================
       2012-monta-tabela section.
       2012.

       exec html

           <input type=button name=botVoltar tabindex=999 value="<- voltar" onclick="Voltar()">

           <table :html-table>
           <tr>
               <td colspan=2><h1 class="txtSuperior">CADASTRO DE VENDEDORES</h1></td>
           </tr>

           <tr>
               <td class="labelCadastro">ID:</td>
               <td><input type=text value="" name=txtIDVendedor tabindex=1 > <input type=button name=botPesquisar value=Pesquisar onclick="Pesquisar()"> </td>
           </tr>

           <tr>
               <td class="labelCadastro">CPF:</td>
               <td><input type=text name=txtCPF tabindex=2 value="" onblur=""></td>
           </tr>

           <tr>
               <td class="labelCadastro">Nome:</td>
               <td><input type=text name=txtNome tabindex=3 value=""></td>
           </tr>

           <tr>
               <td class="labelCadastro">Latitude:</td>
               <td><input type=text name=txtLatitude tabindex=4 value=""></td>
           </tr>

           <tr>
               <td class="labelCadastro">Longitude:</td>
               <td><input type=text name=txtLongitude tabindex=5 value=""></td>
           </tr>

           <tr>
               <td colspan=2>
                   <input type=button name=botNovo tabindex=6 value=Novo onclick="Novo()">
                   <input type=button name=botSalvar tabindex=7 value=Salvar onclick="Salvar()">
                   <input type=button name=botExcluir tabindex=8 value=Excluir onclick="Excluir()">
                   <input type=button name=botImportar tabindex=9 value=Importar onclick="Importar()">
                   <input type=button name=botCancelar tabindex=10 value=Cancelar onclick="Cancelar()">

               </td>
           </tr>



           </table>
       end-exec
            .
       2012-exit.
            exit.
      *>===================================================================================
       2013-scripts-tela section.
       2013.

       exec html
       <script>

           function Cancelar(){
               DisabledBotoes();
               document.formMenus.opcaoCadVend.value = 0;
               document.formMenus.submit();
           }

           function Novo(){
               DisabledBotoes();
               document.formMenus.opcaoCadVend.value = 1;
               document.formMenus.submit();
           }

           function Salvar(){
               DisabledBotoes();
               document.formMenus.opcaoCadVend.value = 2;
               document.all.txtIDVendedor.disabled = false;
               document.formMenus.submit();
           }

           function Pesquisar() {
               DisabledBotoes();
               document.formMenus.opcaoCadVend.value = 3;
               document.formMenus.submit();
           }

           function Excluir(){
               DisabledBotoes();
               document.formMenus.opcaoCadVend.value = 4;
               document.all.txtIDVendedor.disabled = false;
               document.formMenus.submit();
           }

           function Importar() {
               DisabledBotoes();
               document.formMenus.opcaoCadVend.value = 5;
               document.formMenus.submit();
           }

           function ExeImportacao() {
               document.all.botExeImportar.disabled = true;
               document.formMenus.opcaoCadVend.value = 6;
               document.formMenus.submit();
           }

           function DisabledBotoes(){
               document.all.botNovo.disabled = true;
               document.all.botSalvar.disabled = true;
               document.all.botExcluir.disabled = true;
               document.all.botImportar.disabled = true;
               document.all.botPesquisar.disabled = true;
           }

           function Voltar(){
               document.formMenus.opcaoMenus.value = 0;
               document.formMenus.submit();
           }

           function VoltarVend(){
               document.formMenus.opcaoCadVend.value = 0;
               document.formMenus.submit();
           }

       </script>
       end-exec

            .
       2013-exit.
            exit.
      *>===================================================================================
       2020-salvar section.
       2020.
            if f-id-vendedor = 0
               perform 2020-salvar-novo
            else
               perform 2025-atualizar-cliente
            end-if
            .
       2020-exit.
      *>===================================================================================
       2020-salvar-novo section.
       2020.

            open i-o arq-vendedores
            if not ws-operacao-ok
                   string
                       "Erro abertura do arquivo arq-vendedores, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 0000-saida
            end-if

            perform 2021-valida-campos
            if not ws-campos-ok
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            perform 2022-gravar-arquivo-clientes

            close arq-vendedores
            .
       2020-exit.
      *>===================================================================================
       2021-valida-campos section.
       2021.
            move "n"                               to ws-valida-campos
            move spaces                            to ws-campo-erro

            if (f-cpf = 99999999999)
            or (f-cpf = 00000000000)
            or (f-cpf < 100000000)
               string "cpf inv�lido"              into ws-mensagem
               end-string
               if not f-opcao-cad-cli = 6 *> quando n�o � importacao
                      perform 8000-mensagem
               end-if
               exit section
            end-if

            if f-nome equal spaces
               string "Informe o nome do vendedor!"    into ws-mensagem
               end-string
               if not f-opcao-cad-cli = 6 *> quando n�o � importacao
                      perform 8000-mensagem
               end-if
               exit section
            end-if


            *>todo: falta implementar mais valida��es
            move "s"                               to ws-valida-campos
            .

       2021-exit.
            exit.
      *>===================================================================================
       2022-gravar-arquivo-clientes section.
       2022.

            perform 2023-procura-cod-livre
            initialize                             registro-vendedores
            move f-cpf                             to cpf
            move f-nome                            to nome
            move f-latitude                        to latitude
            move f-longitude                       to longitude
            move ws-cod-cliente-livre              to cod-vendedor

            write                                  registro-vendedores

            if not ws-operacao-ok
                   string
                       "Erro ao gravar cliente, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 0000-saida
            else
                   string
                       "Novo vendedor salvo com sucesso, Cliente: " nome
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
            end-if
            .
       2022-exit.
            exit.
      *>===================================================================================
       2023-procura-cod-livre section.
       2023.

            read arq-vendedores next
            if   ws-eof-arquivo
                 move 1                            to ws-cod-cliente-livre
                 exit section
            end-if

            perform 2024-loop-read-clientes        until ws-eof-arquivo
            move cod-vendedor                      to ws-cod-cliente-livre
            add 1                                  to ws-cod-cliente-livre
            .
       2023-exit.
            exit.
      *>===================================================================================
       2024-loop-read-clientes section.
       2024.
             read arq-vendedores next
            .
       2024-exit.
            exit.
      *>===================================================================================
       2025-atualizar-cliente section.
       2025.
            open i-o arq-vendedores

            move f-id-vendedor                      to nome
            read arq-vendedores

            if not ws-operacao-ok
                   string
                       "Erro ao atualizar cliente, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            move f-cpf                             to cpf
            move f-nome                            to nome
            move f-latitude                        to latitude
            move f-longitude                       to longitude

            perform 2021-valida-campos
            if not ws-campos-ok
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            rewrite registro-vendedores

            if not ws-operacao-ok
                   string
                       "Erro ao atualizar cliente, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            else
                   string
                       "Cliente atualizado, cliente: ", nome
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            close arq-vendedores
            .
       2025-exit.
            exit.
      *>===================================================================================
       2030-pesquisar section.
       2030.

            move "n"                               to ws-encontrou-registro

            open i-o arq-vendedores
            if not ws-operacao-ok
                   string
                       "Erro abertura do arquivo arq-vendedores, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 0000-saida
            end-if

            move f-id-vendedor                     to cod-vendedor
            read arq-vendedores
            if   ws-registro-inexistente
                   string
                       "Registro n�o encontrado, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            perform 2031-carrega-dados-tela
            close arq-vendedores
            .
       2030-exit.
            exit.
      *>===================================================================================
       2031-carrega-dados-tela section.
       2031.

            perform 2010-monta-tela-inicial
            move "s"                               to ws-encontrou-registro

       exec html
       <script>

           document.all.txtIDVendedor.value = ":cod-vendedor";
           document.all.txtCPF.value = ":cpf";
           document.all.txtNome.value = ":nome";
           document.all.txtLatitude.value = ":latitude";
           document.all.txtLongitude.value = ":longitude";

       </script>

       end-exec
            .
       2031-exit.
            exit.
      *>===================================================================================
       2040-excluir section.
       2040.
            open i-o arq-vendedores
            move f-id-vendedor                      to cod-vendedor
            read arq-vendedores
            if   ws-registro-inexistente
                   string
                       "Registro n�o encontrado, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            delete arq-vendedores
            string
                "Cliente removido com sucesso, cliente: ", nome
            into ws-mensagem
            end-string
            perform 8000-mensagem
            perform 2010-monta-tela-inicial

            close arq-vendedores
            .
       2040-exit.
            exit.
      *>===================================================================================
       2050-importar section.
       2050.

            perform 2051-tela-importacao
            .
       2050-exit.
            exit.
      *>===================================================================================
       2051-tela-importacao section.
       2051.

            perform 9000-css-pagina
            perform 2013-scripts-tela

       exec html

           <!-- =============================== IMPORTAR ================================= -->
           <input type=button name=botVoltar tabindex=999 value="<- voltar" onclick="VoltarVend()">

           <table :html-table>
               <tr>
               <td colspan=3
                   <p class="txtSuperior"> Importa��o de vendedores </p>
               </td>
               </tr>

               <tr>
                   <td>:lnk-path-import</td>
                   <td><input type=text name=txtArquivo></td>
                   <td><input type=button name=botExeImportar value="Executar" onclick="ExeImportacao()"></td>
               </tr>

           </table>


           <!-- =============================== IMPORTAR FIM ================================= -->
       end-exec

            .
       2051-exit.
            exit.
      *>===================================================================================
       2060-exe-importar section.
       2060.
            move "s"                               to ws-valida-arquivo
            perform 2061-verifica-arquivo
            if not ws-arquivo-ok
               string "Arquivo[" f-arquivo delimited by " ", "] n�o encontrado"
               into ws-mensagem
               end-string

               perform 8000-mensagem
               perform 2051-tela-importacao
               exit section
            end-if

            open input arq-vendedores-importacao
            read arq-vendedores-importacao
            move "s"                               to ws-status-importacao
            move zeros                             to ws-linha-arq
            perform 2062-separa-campos             until ws-eof-arquivo
            close arq-vendedores-importacao

            if   ws-importacao-ok
                 perform 2065-gravar-dados-na-base
                 string "Arquivo[" f-arquivo delimited by " ", "] importado com sucesso"
                 into ws-mensagem
                 end-string

                 perform 8000-mensagem
                 perform 2051-tela-importacao
            else
                 perform 8000-mensagem
                 perform 2051-tela-importacao
            end-if
            .
       2060-exit.
            exit.
      *>===================================================================================
       2061-verifica-arquivo section.
       2061.
            call "CBL_CHECK_FILE_EXIST"      using wid-arq-vendedores-importacao
                                                   ws-file-details
                                         returning ws-status-code

            if (ws-status-code = 14605) *>n�o encontrado
            or (ws-status-code = 14613) *>em branco
                move "n"                           to ws-valida-arquivo
            end-if
            .
       2061-exit.
            exit.
      *>===================================================================================
       2062-separa-campos section.
       2062.
            move zeros                             to ws-campos-importacao
            move zeros                             to ws-qtd-campos-esperados
            add 1                                  to ws-linha-arq

            perform varying ws-ind from 1 by 1     until ws-ind greater 83

               if   campos-importacao(ws-ind:1) equal ";"
                    add 1                 to  ws-qtd-campos-esperados
               end-if

            end-perform

            unstring campos-importacao delimited by ";" into imp-cpf-x
                                                             imp-razao-social-x
                                                             imp-latitude-x
                                                             imp-longitude-x

            move function numval(imp-cpf-x)        to imp-cpf
            move imp-razao-social-x                to imp-razao-social
            move imp-latitude-x                    to imp-latitude
            move imp-longitude-x                   to imp-longitude

            *>para validar-campos
            move imp-cpf                           to f-cpf
            move imp-razao-social-x                to f-nome
            move imp-latitude-x                    to f-latitude
            move imp-longitude-x                   to f-longitude

            perform 2063-validacoes-importacao
            perform 2021-valida-campos
            if not ws-campos-ok
               string "Campos inv�lidos para importa��o, linha: ", ws-linha-arq
                   into ws-mensagem
               end-string
               move "n"                            to ws-status-importacao
            end-if

            if   ws-importacao-ok
                 perform 2064-grava-arq-tmp
            end-if

            read arq-vendedores-importacao next
            .
       2062-exit.
            exit.
      *>===================================================================================
       2063-validacoes-importacao section.
       2063.

            if   ws-qtd-campos-esperados <> 3 *> quantidade de colunas no arquivo
                 string "Layout de arquivo incorreto. Informe no arquivo csv: cpf; Raz�o social; Latitude; Longitude"
                   into ws-mensagem
                 end-string
                 move "n"                          to ws-status-importacao
                 exit section
            end-if

            .
       2063-exit.
            exit.
      *>===================================================================================
       2064-grava-arq-tmp section.
       2064.
            open extend arq-vendedores-tmp

            move zeros                             to tmp-cod-cliente
            move imp-cpf                           to tmp-cpf
            move imp-razao-social                  to tmp-razao-social
            move imp-latitude                      to tmp-latitude
            move imp-longitude                     to tmp-longitude

            write tmp-registro-clientes

            close arq-vendedores-tmp
            .
       2064-exit.
            exit.
      *>===================================================================================
       2065-gravar-dados-na-base section.
       2065.

            initialize registro-vendedores
            initialize tmp-registro-clientes

            open input arq-vendedores-tmp
            open i-o arq-vendedores
            perform 2023-procura-cod-livre

            read arq-vendedores-tmp
            perform 2066-move-tmp-para-arq-vend

            perform 2067-next-arq-vendedores-tmp until ws-eof-arquivo

            close arq-vendedores
            close arq-vendedores-tmp
            .
       2065-exit.
            exit.
      *>===================================================================================
       2066-move-tmp-para-arq-vend section.
       2066.

            *>inspect tmp-latitude  replacing all "p" by "-"
            *>inspect tmp-longitude replacing all "p" by "-"

            move ws-cod-cliente-livre              to cod-vendedor
            move tmp-cpf                           to cpf
            move tmp-razao-social                  to nome
            move tmp-latitude                      to latitude
            move tmp-longitude                     to longitude
            .
       2066-exit.
            exit.

      *>===================================================================================
       2067-next-arq-vendedores-tmp section.
       2067.

            write registro-vendedores
            add 1                                  to ws-cod-cliente-livre

            read arq-vendedores-tmp next
            perform 2066-move-tmp-para-arq-vend
            .
       2067-exit.
            exit.
      *>===================================================================================
       3000-finalizacao section.
       3000.

            perform 3010-fechamento-arquivos
            perform 3020-controles-de-tela

            .
       3000-exit.
            exit.
      *>===================================================================================
       3010-fechamento-arquivos section.
       3010.

            close arq-vendedores
            close arq-vendedores-importacao
            close arq-vendedores-tmp

            call "CBL_DELETE_FILE" using wid-arq-vendedores-tmp

            .
       3010-exit.
            exit.
      *>===================================================================================
       3020-controles-de-tela section.
       3020.

       exec html
       <script>

           if( ':f-opcao-cad-vend' == '00' ) {
               document.all.txtIDVendedor.focus();
           }

           if( ':f-opcao-cad-vend' == '01' ) {
               document.all.txtIDVendedor.value = 0;
               document.all.txtIDVendedor.disabled = true;
               document.all.txtCPF.focus();
           }

           if( ':f-opcao-cad-vend' == '03' ) {
               document.all.txtIDVendedor.focus();

               if ( ':ws-encontrou-registro' == 's' ) {
                   document.all.txtIDVendedor.disabled = true;
                   document.all.txtCPF.focus();
               }
           }

           if( ':f-opcao-cad-vend' == '05' ) {
               document.all.txtArquivo.focus();
           }

       </script>
       end-exec

            .
       3020-exit.
            exit.

       copy .\copys\ROTINAS-PADRAO.cpy.


