      $set preprocess (htmlpp) endp
      $set sourceformat"free"

      *>===================================================================================
       identification division.
       program-id.    CadCliente.
      *>===================================================================================

      *>   -----------------------------------------------------
      *>               Programa cad clientes
      *>   -----------------------------------------------------

      *>===================================================================================
       environment division.
       special-names. decimal-point is comma.
      *>===================================================================================
       input-output section.
       file-control.

       copy .\copys\SEL-ARQ-CLIENTES.cpy.

       select arq-clientes-importacao assign to disk wid-arq-clientes-importacao
           organization    is line sequential
           access mode     is sequential
           file status     is ws-resultado-acesso.

       select arq-clientes-tmp assign to disk wid-arq-clientes-tmp
           organization    is line sequential
           access mode     is sequential
           file status     is ws-resultado-acesso.

      *>===================================================================================
       data division.

       copy .\copys\FD-ARQ-CLIENTES.cpy.

       fd arq-clientes-importacao.
       01 imp-registro-clientes.
           02 campos-importacao                    pic x(92).

       fd arq-clientes-tmp.
       01 tmp-registro-clientes.
           02 tmp-cod-cliente                      pic 9(07).
           02 tmp-cnpj                             pic 9(14).
           02 tmp-razao-social                     pic x(40).
           02 tmp-latitude                         pic x(12).
           02 tmp-longitude                        pic x(12).
           02 tmp-FK-cod-vendedor                  pic 9(07).

      *>===================================================================================
       working-storage section.
       78 versao                                   value "1.8".

       copy .\copys\WS-PADRAO.cpy.
       copy .\copys\LNK.cpy.

       01 ws-campos-de-trabalho.
           02 ws-valida-campos                     pic x(01).
               88 ws-campos-ok                     value "s" "S".
           02 ws-campo-erro                        pic x(30).
           02 ws-cod-cliente-livre                 pic 9(07).
           02 ws-encontrou-registro                pic x(01).
           02 ws-valida-arquivo                    pic x(01).
               88 ws-arquivo-ok                    value "s" "S".
           02 ws-qtd-campos-esperados              pic 9(01).
           02 ws-status-importacao                 pic x(01).
               88 ws-importacao-ok                 value "s" "S".
           02 ws-ind                               pic 9(05) value zeros.
           02 ws-latitude-x                        pic x(12).
           02 ws-longitude-x                       pic x(12).
           02 ws-linha-arq                         pic 9(07).



       01 ws-campos-importacao.
           02 imp-cnpj-x                           pic x(14).
           02 imp-razao-social-x                   pic x(40).
           02 imp-latitude-x                       pic x(12).
           02 imp-longitude-x                      pic x(12).
           02 imp-fk-cod-vendedor-x                pic x(07).

           02 imp-cnpj                             pic 9(14).
           02 imp-razao-social                     pic x(40).
           02 imp-latitude                         pic x(12).
           02 imp-longitude                        pic x(12).
           02 imp-fk-cod-vendedor                  pic 9(07).


      *>===================================================================================
       local-storage section.

      *>===================================================================================
       procedure division using cgi-input lnk-rotinas.

      *>===================================================================================
       0000-controle section.
       0000.
            perform 1000-inicializacao.
            perform 2000-processamento.
            perform 3000-finalizacao.
       0000-saida.
            exit program
            stop run.

      *>===================================================================================
       1000-inicializacao section.
       1000.

            move spaces                            to cgi-input
            initialize                             cgi-input
            accept                                 cgi-input

            perform 1010-assign-arquivos
            perform 1011-cricao-arquivos
            perform 1012-teste-abertuta-arquivos
            .
       1000-exit.
            exit.
      *>===================================================================================
       1010-assign-arquivos section.
       1010.

            string
               lnk-path-dat                        delimited by " ",
               "ARQ-CLIENTES"                      delimited by " "
                                                   into wid-arq-clientes
            end-string

            string
               lnk-path-import                     delimited by " ",
               f-arquivo                           delimited by " "
                                                   into wid-arq-clientes-importacao
            end-string

            string
               lnk-path-tmp                        delimited by " ",
               "tmp_importar_"                      delimited by " ",
               f-arquivo                           delimited by " ",
               "_",
               lnk-data-hora-maquina               delimited by " ",
                                                   into wid-arq-clientes-tmp
            end-string

            .
       1010-exit.
            exit.
      *>===================================================================================
       1011-cricao-arquivos section.
       1011.

            open i-o arq-clientes
            close arq-clientes

            .
       1010-exit.
            exit.
      *>===================================================================================
       1012-teste-abertuta-arquivos section.
       1012.

            open i-o arq-clientes
            if not ws-operacao-ok

                   string
                       "Erro abertura do arquivo arq-clientes, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 0000-saida
            end-if
            close arq-clientes

            .
       1012-exit.
            exit.
      *>===================================================================================
       2000-processamento section.
       2000.

            evaluate f-opcao-cad-cli
            when 0
               perform 2010-monta-tela-inicial
            when 1
               perform 2010-monta-tela-inicial
            when 2
               perform 2020-salvar
            when 3
               perform 2030-pesquisar
            when 4
               perform 2040-excluir
            when 5
               perform 2050-importar
            when 6
               perform 2060-exe-importar
            when other
               move "opcao inv�lida" to ws-mensagem
               perform 8000-mensagem
               perform 0000-saida
            end-evaluate

            .
       2000-exit.
            exit.
      *>===================================================================================
       2010-monta-tela-inicial section.
       2010.

       exec html
           <html><title>Cadastro de Clientes</title>
       end-exec

            perform 9000-css-pagina
            perform 2012-monta-tabela
            perform 2013-scripts-tela

       exec html
           </html>
       end-exec
            .
       2010-exit.
            exit.
      *>===================================================================================
       2012-monta-tabela section.
       2012.

       exec html

           <input type=button name=botVoltar tabindex=999 value="<- voltar" onclick="Voltar()">

           <table :html-table>
           <tr>
               <td colspan=2><h1 class="txtSuperior">CADASTRO DE CLIENTES</h1></td>
           </tr>

           <tr>
               <td class="labelCadastro">ID:</td>
               <td><input type=text value="" name=txtID tabindex=1 > <input type=button name=botPesquisar value=Pesquisar onclick="Pesquisar()"> </td>
           </tr>

           <tr>
               <td class="labelCadastro">CNPJ:</td>
               <td><input type=text name=txtCNPJ tabindex=2 value="" onblur=""></td>
           </tr>

           <tr>
               <td class="labelCadastro">Raz�o Social:</td>
               <td><input type=text name=txtRazaoSocial tabindex=3 value=""></td>
           </tr>

           <tr>
               <td class="labelCadastro">Latitude:</td>
               <td><input type=text name=txtLatitude tabindex=4 value=""></td>
           </tr>

           <tr>
               <td class="labelCadastro">Longitude:</td>
               <td><input type=text name=txtLongitude tabindex=5 value=""></td>
           </tr>

           <tr>
               <td class="labelCadastro">Vendedor:</td>
               <td><input type=text name=txtVendedor tabindex=6 value=""></td>
           </tr>

           <tr>
               <td colspan=2>
                   <input type=button name=botNovo tabindex=101 value=Novo onclick="Novo()">
                   <input type=button name=botSalvar tabindex=102 value=Salvar onclick="Salvar()">
                   <input type=button name=botExcluir tabindex=103 value=Excluir onclick="Excluir()">
                   <input type=button name=botImportar tabindex=104 value=Importar onclick="Importar()">
                   <input type=button name=botCancelar tabindex=105 value=Cancelar onclick="Cancelar()">

               </td>
           </tr>



           </table>
       end-exec
            .
       2012-exit.
            exit.
      *>===================================================================================
       2013-scripts-tela section.
       2013.

       exec html
       <script>

           function Cancelar(){
               DisabledBotoes();
               document.formMenus.opcaoCadCli.value = 0;
               document.formMenus.submit();
           }

           function Novo(){
               DisabledBotoes();
               document.formMenus.opcaoCadCli.value = 1;
               document.formMenus.submit();
           }

           function Salvar(){
               DisabledBotoes();
               document.formMenus.opcaoCadCli.value = 2;
               document.all.txtID.disabled = false;
               document.formMenus.submit();
           }

           function Pesquisar() {
               DisabledBotoes();
               document.formMenus.opcaoCadCli.value = 3;
               document.formMenus.submit();
           }

           function Excluir(){
               DisabledBotoes();
               document.formMenus.opcaoCadCli.value = 4;
               document.all.txtID.disabled = false;
               document.formMenus.submit();
           }

           function Importar() {
               DisabledBotoes();
               document.formMenus.opcaoCadCli.value = 5;
               document.formMenus.submit();
           }

           function ExeImportacao() {
               document.all.botExeImportar.disabled = true;
               document.formMenus.opcaoCadCli.value = 6;
               document.formMenus.submit();
           }

           function DisabledBotoes(){
               document.all.botNovo.disabled = true;
               document.all.botSalvar.disabled = true;
               document.all.botExcluir.disabled = true;
               document.all.botImportar.disabled = true;
               document.all.botPesquisar.disabled = true;
           }

           function Voltar(){
               document.formMenus.opcaoMenus.value = 0;
               document.formMenus.submit();
           }

           function VoltarCli(){
               document.formMenus.opcaoCadCli.value = 0;
               document.formMenus.submit();
           }

       </script>
       end-exec

            .
       2013-exit.
            exit.
      *>===================================================================================
       2020-salvar section.
       2020.
            if f-id-cliente = 0
               perform 2020-salvar-novo
            else
               perform 2025-atualizar-cliente
            end-if
            .
       2020-exit.
      *>===================================================================================
       2020-salvar-novo section.
       2020.

            open i-o arq-clientes
            if not ws-operacao-ok
                   string
                       "Erro abertura do arquivo arq-clientes, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 0000-saida
            end-if

            perform 2021-valida-campos
            if not ws-campos-ok
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            perform 2022-gravar-arquivo-clientes

            close arq-clientes
            .
       2020-exit.
      *>===================================================================================
       2021-valida-campos section.
       2021.
            move "n"                               to ws-valida-campos
            move spaces                            to ws-campo-erro

            if (f-cnpj = 99999999999999)
            or (f-cnpj = 00000000000000)
            or (f-cnpj < 100000000000)
               string "CNPJ inv�lido"              into ws-mensagem
               end-string
               if not f-opcao-cad-cli = 6 *> quando n�o � importacao
                      perform 8000-mensagem
               end-if
               exit section
            end-if

            if f-razao-social equal spaces
               string "Informe a raz�o social!"    into ws-mensagem
               end-string
               if not f-opcao-cad-cli = 6 *> quando n�o � importacao
                      perform 8000-mensagem
               end-if
               exit section
            end-if


            *>todo: falta implementar mais valida��es
            move "s"                               to ws-valida-campos
            .

       2021-exit.
            exit.
      *>===================================================================================
       2022-gravar-arquivo-clientes section.
       2022.

            perform 2023-procura-cod-livre
            initialize                             registro-clientes
            move f-cnpj                            to cnpj
            move f-razao-social                    to razao-social
            move f-latitude                        to latitude
            move f-longitude                       to longitude
            move f-fk-cod-vendedor                 to FK-cod-vendedor
            move ws-cod-cliente-livre              to cod-cliente

            write                                  registro-clientes

            if not ws-operacao-ok
                   string
                       "Erro ao gravar cliente, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 0000-saida
            else
                   string
                       "Novo cliente salvo com sucesso, Cliente: " razao-social
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
            end-if
            .
       2022-exit.
            exit.
      *>===================================================================================
       2023-procura-cod-livre section.
       2023.

            read arq-clientes next
            if   ws-eof-arquivo
                 move 1                            to ws-cod-cliente-livre
                 exit section
            end-if

            perform 2024-loop-read-clientes until ws-eof-arquivo
            move cod-cliente                       to ws-cod-cliente-livre
            add 1                                  to ws-cod-cliente-livre
            .
       2023-exit.
            exit.
      *>===================================================================================
       2024-loop-read-clientes section.
       2024.
             read arq-clientes next
            .
       2024-exit.
            exit.
      *>===================================================================================
       2025-atualizar-cliente section.
       2025.
            open i-o arq-clientes

            move f-id-cliente                      to cod-cliente
            read arq-clientes

            if not ws-operacao-ok
                   string
                       "Erro ao atualizar cliente, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            move f-cnpj                            to cnpj
            move f-razao-social                    to razao-social
            move f-latitude                        to latitude
            move f-longitude                       to longitude
            move f-fk-cod-vendedor                 to FK-cod-vendedor

            perform 2021-valida-campos
            if not ws-campos-ok
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            rewrite registro-clientes

            if not ws-operacao-ok
                   string
                       "Erro ao atualizar cliente, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            else
                   string
                       "Cliente atualizado, cliente: ", razao-social
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            close arq-clientes
            .
       2025-exit.
            exit.
      *>===================================================================================
       2030-pesquisar section.
       2030.

            move "n"                               to ws-encontrou-registro

            open i-o arq-clientes
            if not ws-operacao-ok
                   string
                       "Erro abertura do arquivo arq-clientes, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 0000-saida
            end-if

            move f-id-cliente                      to cod-cliente
            read arq-clientes
            if   ws-registro-inexistente
                   string
                       "Registro n�o encontrado, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            perform 2031-carrega-dados-tela
            close arq-clientes
            .
       2030-exit.
            exit.
      *>===================================================================================
       2031-carrega-dados-tela section.
       2031.

            perform 2010-monta-tela-inicial
            move "s"                               to ws-encontrou-registro

       exec html
       <script>

           document.all.txtID.value = ":cod-cliente";
           document.all.txtCNPJ.value = ":cnpj";
           document.all.txtRazaoSocial.value = ":razao-social";
           document.all.txtLatitude.value = ":latitude";
           document.all.txtLongitude.value = ":longitude";
           document.all.txtVendedor.value = ":FK-cod-vendedor";


       </script>

       end-exec
            .
       2031-exit.
            exit.
      *>===================================================================================
       2040-excluir section.
       2040.
            open i-o arq-clientes
            move f-id-cliente                      to cod-cliente
            read arq-clientes
            if   ws-registro-inexistente
                   string
                       "Registro n�o encontrado, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            delete arq-clientes
            string
                "Cliente removido com sucesso, cliente: ", razao-social
            into ws-mensagem
            end-string
            perform 8000-mensagem
            perform 2010-monta-tela-inicial

            close arq-clientes
            .
       2040-exit.
            exit.
      *>===================================================================================
       2050-importar section.
       2050.

            perform 2051-tela-importacao
            .
       2050-exit.
            exit.
      *>===================================================================================
       2051-tela-importacao section.
       2051.

            perform 9000-css-pagina
            perform 2013-scripts-tela

       exec html

           <!-- =============================== IMPORTAR ================================= -->
           <input type=button name=botVoltar tabindex=999 value="<- voltar" onclick="VoltarCli()">
           <table :html-table>
               <tr>
               <td colspan=3
                   <p class="txtSuperior"> Importa��o de clientes </p>
               </td>
               </tr>

               <tr>
                   <td>:lnk-path-import</td>
                   <td><input type=text name=txtArquivo></td>
                   <td><input type=button name=botExeImportar value="Executar" onclick="ExeImportacao()"></td>
               </tr>

           </table>


           <!-- =============================== IMPORTAR FIM ================================= -->
       end-exec

            .
       2051-exit.
            exit.
      *>===================================================================================
       2060-exe-importar section.
       2060.
            move "s"                               to ws-valida-arquivo
            perform 2061-verifica-arquivo
            if not ws-arquivo-ok
               string "Arquivo[" f-arquivo delimited by " ", "] n�o encontrado"
               into ws-mensagem
               end-string

               perform 8000-mensagem
               perform 2051-tela-importacao
               exit section
            end-if

            open input arq-clientes-importacao
            read arq-clientes-importacao
            move "s"                               to ws-status-importacao
            move zeros                             to ws-linha-arq
            perform 2062-separa-campos             until ws-eof-arquivo
            close arq-clientes-importacao

            if   ws-importacao-ok
                 perform 2065-gravar-dados-na-base
                 string "Arquivo[" f-arquivo delimited by " ", "] importado com sucesso"
                 into ws-mensagem
                 end-string

                 perform 8000-mensagem
                 perform 2051-tela-importacao
            else
                 perform 8000-mensagem
                 perform 2051-tela-importacao
            end-if
            .
       2060-exit.
            exit.
      *>===================================================================================
       2061-verifica-arquivo section.
       2061.
            call "CBL_CHECK_FILE_EXIST"      using wid-arq-clientes-importacao
                                                   ws-file-details
                                         returning ws-status-code

            if (ws-status-code = 14605) *>n�o encontrado
            or (ws-status-code = 14613) *>em branco
                move "n"                           to ws-valida-arquivo
            end-if
            .
       2061-exit.
            exit.
      *>===================================================================================
       2062-separa-campos section.
       2062.
            move zeros                             to ws-campos-importacao
            move zeros                             to ws-qtd-campos-esperados
            add 1                                  to ws-linha-arq

            perform varying ws-ind from 1 by 1     until ws-ind greater 92

               if   campos-importacao(ws-ind:1) equal ";"
                    add 1                 to  ws-qtd-campos-esperados
               end-if

            end-perform

            unstring campos-importacao delimited by ";" into imp-cnpj-x
                                                             imp-razao-social-x
                                                             imp-latitude-x
                                                             imp-longitude-x
                                                             imp-fk-cod-vendedor-x

            move function numval(imp-cnpj-x)               to imp-cnpj
            move imp-razao-social-x                        to imp-razao-social
            move imp-latitude-x                            to imp-latitude
            move imp-longitude-x                           to imp-longitude
            move function numval(imp-fk-cod-vendedor-x)    to imp-fk-cod-vendedor

            *>para validar-campos
            move imp-cnpj                          to f-cnpj
            move imp-razao-social-x                to f-razao-social
            move imp-latitude-x                    to f-latitude
            move imp-longitude-x                   to f-longitude
            move imp-fk-cod-vendedor-x             to f-fk-cod-vendedor

            perform 2063-validacoes-importacao
            perform 2021-valida-campos
            if not ws-campos-ok
               string "Campos inv�lidos para importa��o, linha: ", ws-linha-arq
                   into ws-mensagem
               end-string
               move "n"                            to ws-status-importacao
            end-if

            if   ws-importacao-ok
                 perform 2064-grava-arq-tmp
            end-if

            read arq-clientes-importacao next
            .
       2062-exit.
            exit.
      *>===================================================================================
       2063-validacoes-importacao section.
       2063.

            if   ws-qtd-campos-esperados <> 4 *> quantidade de colunas no arquivo
                 string "Layout de arquivo incorreto. Informe no arquivo csv: CNPJ; Raz�o social; Latitude; Longitude"
                   into ws-mensagem
                 end-string
                 move "n"                          to ws-status-importacao
                 exit section
            end-if

            .
       2063-exit.
            exit.
      *>===================================================================================
       2064-grava-arq-tmp section.
       2064.
            open extend arq-clientes-tmp

            move zeros                             to tmp-cod-cliente
            move imp-cnpj                          to tmp-cnpj
            move imp-razao-social                  to tmp-razao-social
            move imp-latitude                      to tmp-latitude
            move imp-longitude                     to tmp-longitude
            move imp-fk-cod-vendedor               to tmp-fk-cod-vendedor

            write tmp-registro-clientes

            close arq-clientes-tmp
            .
       2064-exit.
            exit.
      *>===================================================================================
       2065-gravar-dados-na-base section.
       2065.

            initialize registro-clientes
            initialize tmp-registro-clientes

            open input arq-clientes-tmp
            open i-o arq-clientes
            perform 2023-procura-cod-livre

            read arq-clientes-tmp
            perform 2066-move-tmp-para-arq-clientes

            perform 2067-next-arq-clientes-tmp until ws-eof-arquivo

            close arq-clientes
            close arq-clientes-tmp
            .
       2065-exit.
            exit.
      *>===================================================================================
       2066-move-tmp-para-arq-clientes section.
       2066.

            *>inspect tmp-latitude  replacing all "p" by "-"
            *>inspect tmp-longitude replacing all "p" by "-"

            move ws-cod-cliente-livre              to cod-cliente
            move tmp-cnpj                          to cnpj
            move tmp-razao-social                  to razao-social
            move tmp-latitude                      to latitude
            move tmp-longitude                     to longitude
            move tmp-FK-cod-vendedor               to FK-cod-vendedor
            .
       2066-exit.
            exit.

      *>===================================================================================
       2067-next-arq-clientes-tmp section.
       2067.

            write registro-clientes
            add 1                                  to ws-cod-cliente-livre

            read arq-clientes-tmp next
            perform 2066-move-tmp-para-arq-clientes
            .
       2067-exit.
            exit.
      *>===================================================================================
       3000-finalizacao section.
       3000.

            perform 3010-fechamento-arquivos
            perform 3020-controles-de-tela

            .
       3000-exit.
            exit.
      *>===================================================================================
       3010-fechamento-arquivos section.
       3010.

            close arq-clientes
            close arq-clientes-importacao
            close arq-clientes-tmp

            call "CBL_DELETE_FILE" using wid-arq-clientes-tmp

            .
       3010-exit.
            exit.
      *>===================================================================================
       3020-controles-de-tela section.
       3020.

       exec html
       <script>

           if( ':f-opcao-cad-cli' == '00' ) {
               document.all.txtID.focus();
           }

           if( ':f-opcao-cad-cli' == '01' ) {
               document.all.txtID.value = 0;
               document.all.txtID.disabled = true;
               document.all.txtCNPJ.focus();
           }

           if( ':f-opcao-cad-cli' == '03' ) {
               document.all.txtID.focus();

               if ( ':ws-encontrou-registro' == 's' ) {
                   document.all.txtID.disabled = true;
                   document.all.txtCNPJ.focus();
               }
           }

           if( ':f-opcao-cad-cli' == '05' ) {
               document.all.txtArquivo.focus();
           }

       </script>
       end-exec

            .
       3020-exit.
            exit.
      *>===================================================================================

       copy .\copys\ROTINAS-PADRAO.cpy.


