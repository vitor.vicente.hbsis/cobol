       fd arq-clientes.
       01 registro-clientes.
           02 cod-cliente                          pic 9(07).
           02 cnpj                                 pic 9(14).
           02 razao-social                         pic x(40).
           02 latitude                             pic x(12).
           02 longitude                            pic x(12).
           02 FK-cod-vendedor                      pic 9(07).
